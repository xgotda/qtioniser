Mgf
=====

Mgf stands for *mascot generic format* and is a type of file that is generated following a double mass spectrometry (MS/MS) analysis. Usually each mgf file contains the result of several precursor peptides with different masses. It is the expected format for processing within the Ioniser.

This list is the second part of the mass spectrometer (MS) where the precursor peptide has been fragmented into ions. The top lines are a summary about the peptide: mass to charge ratio (mass/charge = m/z) and intensity (relative abundance), charge, retention time etc.. Subsequent rows denote each fragment's m/z and intensity respectively.

Example output for one peptide:: 

    BEGIN IONS
    TITLE=File35959 Spectrum4 scans: 1998
    PEPMASS=962.87366 49449.23828
    CHARGE=4+
    RTINSECONDS=703
    SCANS=1998
    121.02872 5330.14
    138.05510 60335.9
    139.05846 2910.22
    151.03906 922.584
    204.08682 66147.9
    205.09036 5233.9
    214.07114 1166.55
    215.05540 2898.87
    232.08102 883.794
    275.09613 4814.18
    292.10260 12673
    293.10672 1073
    355.06897 945.847
    359.02771 1005.67
    366.13947 46588.9
    367.14365 6423.05
    411.07169 807.433
    411.10104 1014.95
    825.40460 1114.8
    881.42157 1010.11
    1027.47668 2953.36
    1028.47888 948.384
    1189.52710 1412.63
    1863.77393 1392.98
    1878.75916 3946.66
    1879.75464 2924.26
    1880.75415 2516.38
    1881.74561 1071.04
    2390.95508 1325.83
    2391.94360 1260.81
    END IONS
