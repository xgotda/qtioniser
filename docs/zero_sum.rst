Zero sum 
=========

This option allows for the in/exclusion of results (rows) where the sum of the intensities for glycans and peptides respectively equals zero. It also includes doubly and triply charged peptides. 

.. image:: zero_sum_red.jpg

Example output with only the scan, glycan and peptide values (amended for readability). ::

    SCAN	147.0652	204.0867	366.1395	1786.9487	1990.0281	2136.086
    1998	0	        0   	        0	        46588.9	        0	        0   
    2001	0               81889.7	        57452.5	        0   	        0	        0
    2002	0	        42380.9	        24336.4	        0	        0	        0
    23011	0	        229351.0	74413.9	        7915.43	        50445.4         18208.4
    22566	0	        1224460.0	550713.0	21839.0	        128304.0	71864.5


Zero sum glycans
------------------

Should the option be unticked for glycans, the row with scan number 1998 would not be included as the sum of the intensities for the values 147.0652, 204.0867 and 366.1395 is zero. ::

    SCAN	147.0652	204.0867	366.1395	1786.9487	1990.0281	2136.086
    2001	0               81889.7	        57452.5	        0   	        0	        0
    2002	0	        42380.9	        24336.4	        0	        0	        0
    23011	0	        229351.0	74413.9	        7915.43	        50445.4         18208.4
    22566	0	        1224460.0	550713.0	21839.0	        128304.0	71864.5


Zero sum peptides
------------------

Should the option be unticked for peptides, the rows with the scan numbers 2001 and 2002 would not be included. ::

    SCAN	147.0652	204.0867	366.1395	1786.9487	1990.0281	2136.086
    1998	0	        0   	        0	        46588.9	        0	        0   
    23011	0	        229351.0	74413.9	        7915.43	        50445.4         18208.4
    22566	0	        1224460.0	550713.0	21839.0	        128304.0	71864.5



Zero sum for both
------------------

If neither glycan or peptide options are selected, only rows with scan numbers 23011 and 22566 would be included as these are the only ones that have both glycan and peptide values (per row). ::

    SCAN	147.0652	204.0867	366.1395	1786.9487	1990.0281	2136.086
    23011	0	        229351.0	74413.9	        7915.43	        50445.4         18208.4
    22566	0	        1224460.0	550713.0	21839.0	        128304.0	71864.5

