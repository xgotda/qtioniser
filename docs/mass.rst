Calculated Mass
================

The actual mass of the peptide is calculated using pepmass (m/z) and charge (z).::

    m/z = (M + z * H) / z
    M = z * (m/z - H)

    
Where H = 1.00727 and is the mass of a positive hydrogen atom (|H+|).

.. |H+| replace:: H\ :sup:`+`
