.yaml
===========

You can save parameters to a file and load them from the saved file using *File -> Save parameters to file / Load parameters from file*. This way you don't have to enter the values manually every time for the same or similar values. 

**Please note that the file name extension must be .yaml!**

.. image:: main_menu_red.jpg

Example of the structure inside a .yaml file::

    glycans:
    - 147.0652
    - 204.0867
    - 366.1395
    glycan_ppm: 5
    input_file: C:\Users\bob\Documents\mgfFiles\small.mgf
    output_file: C:\Users\bob\Code\ba
    peptide_ppm: 5
    peptides:
    - 1786.9487
    - 1990.0281
    - 2136.086
    triply_charged: true
    doubly_charged: true

As always, all glycan and peptide values are |MH+|.

.. |MH+| replace:: MH\ :sup:`+`
