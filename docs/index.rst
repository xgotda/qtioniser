.. Ioniser documentation master file, created by
   sphinx-quickstart on Mon Jul 29 11:26:13 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Ioniser Documentation
=======================================

This is where all of the Ioniser magic is explained!

About Ioniser
-------------
the Ioniser is a piece of software to process output files from a mass spectrometry .mgf files. It allows for the search of peptides based on their masses within a specific tolerance. 



TOPICS:
-------------
.. toctree::
   :maxdepth: 2

   mgf
   output
   isotopes
   multicharged
   ppm
   mass
   zero_sum
   yaml

..  - searching
    - calculated mass
    - glycans vs peptides


..  Indices and tables
    -------------------
    * :ref:`search`
    * :ref:`modindex`
    * :ref:`genindex`
