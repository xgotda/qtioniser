Ppm 
========================

'ppm' stands for parts per million and is used to calculate the accuracy tolerance for each individual m/z value. ::

   (propose m/z is m_z in this calculation)

   tolerance = (m_z * ppm)/1000000
