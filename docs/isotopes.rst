Isotopes
=========

Sometimes the value we are looking for can be present with one or more isotopic peaks. How much they will **differ** by is dependent upon the charge of the ion. The difference is 1/z where z is the charge of the ion. ::
    
    1 mass unit:
        1/1 for singly charged
        1/2 for doubly charged 
        1/3 for triply charged ions 

the Ioniser searches first for the initial value we are interested in within the tolerance as calculated using ppm. If this is found, it then looks at the following two values and if they differ by 1 mass unit from each other, the highest intensity out of the three is chosen. The current tolerance inbetween isotopes is set to **0.01**.

Example: ::
    
    Value we are looking for: 1786.9487

    mgf file: 
    ...
    1770.90625 2685.92
    1771.92700 1051.92
    1786.94458 7659.21
    1787.94324 7915.43
    1788.96338 5814.84
    1869.96692 3488.96
    1870.97864 4163.69
    ...

In the above example, *1787.94324* is correctly identified as an isotope to the singly charged *1786.94458*. The isotope's the higher intensity will be reported.
