Multiply Charged Ions
======================

During MS/MS, as the precursor peptide is fragmented, one or more cations (usually |H+|) will bond with a fragment ion. If the ion has only one cation, it is singly charged. If it has two, it is a doubly charged ion and so on. These are the multi-charged ions.

For singly charged ions, the m/z value equals mass (as m/z = mass/charge). For multi-charged ions however, the m/z value decreases in proportion to the charge. 

.. math:: m/z = \frac{Mass + H^+}{charge}


In the Ioniser the theoretical n-charged (doubly or triply) mass of a m/z value is calculated (see below) and then treated as any other value that is being searched. This includes having its own tolerance based off ppm and looking for isotopes before selecting the highest intensity. 

.. math:: 
    \frac{m/z + H^+ *(n-1)}{n}

*where n = charge*

The option to include doubly or triply charged ions in the search is controlled by ticking the options *doubly* and *triply* respectively on the **Peptides - Charged** section. 

    .. image:: peptides_red.jpg


.. |H+| replace:: H\ :sup:`+`
