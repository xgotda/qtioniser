Output
=======

The output file contains one line per precursor peptide in which any matches have been found. The rows are tabulated (separated by tabs) and contain:

    - the scan number (SCAN), 
    - precursor peptide m/z (MZ), 
    - precursor peptide charge (CHARGE), 
    - the calculated mass (Calc MASS), 
    - retention time (RT), 
    - the maximum intensity found out of the m/z values searched for (MAX Intens), 
    - the number of fragments found (# found) 
    - the intensity of each m/z value searched for. 

Doubly and triply charged values have a postfix of '_2' and '_3' respectively. See example below (amended for readability). ::

    SCAN	MZ  	    CHARGE	Calc MASS	RT	MAX Intens	`#` found	147.0652	204.0867	366.1395	1786.9487	1786.9487_2	1786.9487_3	1990.0281	1990.0281_2	1990.0281_3	2136.086	2136.086_2	2136.086_3	
    1998	962.87366	4	3847.46553	703	    66147.9	    2	            0	        66147.9	        46588.9	        0	        0	        0	        0	        0	        0   	        0	        0	        0
    2001	1016.06757	3	3045.18088	704	    81889.7	    2	            0           81889.7	        57452.5	        0	        0	        0	        0	        0	        0	        0   	        0	        0
    2002	976.10248	4	3900.38081	704	    44667.5	    2	            0	        42380.9	        24336.4	        0	        0	        0	        0	        0	        0	        0   	        0	        0
    23011	1118.52161	3	3352.54300	4479        229351.0	    5	            0	        229351.0	74413.9	        7915.43	        0	        0	        50445.4         0	        0	        18208.4	        0	        0
    22566	1350.93750	3	4049.79067	4412 	    1224460.0	    7	            0	        1224460.0	550713.0	21839.0	        0	        9999.9	        128304.0	4371.17	        0	        71864.5	        0	        0
