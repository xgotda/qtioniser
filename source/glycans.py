#!/usr/bin/env python3

from PyQt5 import QtWidgets, QtCore
import glycan_ui as gu


class GlycanWindow(QtWidgets.QDialog, gu.Ui_glycans_dlgbx):
    def __init__(self, main_window):
        super().__init__(main_window)
        self.setupUi(self)

        self.parent = main_window
        incoming_vals = main_window.glycans_values.toPlainText().split('\n')
    #TODO: pretick incoming values.
        self.values = set(incoming_vals)
        self.Fuc.stateChanged.connect(self.get_value)
        self.Hex.stateChanged.connect(self.get_value)
        self.HexA.stateChanged.connect(self.get_value)
        self.HexNAc.stateChanged.connect(self.get_value)
        self.HexHexNAc.stateChanged.connect(self.get_value)
        self.HexHexNAcFuc.stateChanged.connect(self.get_value)
        self.NeuAc.stateChanged.connect(self.get_value)
        self.NeuAcH2O.stateChanged.connect(self.get_value)
        self.NeuGc.stateChanged.connect(self.get_value)
        self.addGlycans_btn.clicked.connect(self.send_glycans)


    def get_value(self, state):
        text = self.sender().text().split(' ')[0]
        if state == QtCore.Qt.Checked:
            self.values.add(text)
        else:
            self.values.discard(text)

    def send_glycans(self):
        """ Adds the ticked values to glycans text box.
        """
        self.parent.statusbar.showMessage("Glycans added", 2500)
        
        for val in self.values:
            self.parent.glycans_values.appendPlainText(val)
        self.parent.update_box(self.parent.glycans_values)
        self.hide()
