#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 10:51:30 2018
@author: xgotda
"""

__author__ = "D. Gotlib"

import math
import staticVariables as sV
import PeptideClass as p


''' -----------------------------------------------------------
        Functions for file and string processing.
    ----------------------------------------------------------- '''

def processIons(toPrint, search):
    """ Create a multi-array out of the output to be used in nparray later.
    """
    aRow = []
    allRows = []

    for ion in toPrint:
        aRow = [ion.scanNo, ion.pepmass.mz, ion.charge,
                ion.Mass, ion.RT, ion.MaxInts, ion.fragmentCount]
        fList = ion.fragments
        for o in search.oxo_list:
            if fList.get(o, False):
                aRow.append(fList[o][1])
            else:
                aRow.append(0)

        # Add value if found for each value in theSearch list, print 0 if not found. 
        # Same rule for doubly and triply but must be checked separately 
        # as should only print anything if option is selected.
        for ap in search.pp_list:
            if fList.get(ap, False) and fList[ap].get(sV._single, False):
                aRow.append(fList[ap][sV._single])
            else:
                aRow.append(0)

            if search.doubly:
                if (fList.get(ap, False) and fList[ap].get(sV._double, False)):
                    aRow.append(fList[ap][sV._double])
                else:
                    aRow.append(0)
            
            if search.triply:
                if (fList.get(ap, False) and fList[ap].get(sV._triple, False)):
                    aRow.append(fList[ap][sV._triple])
                else:
                    aRow.append(0)
       
        allRows.append(aRow)
    return allRows

def np_to_file(file_name, np_list):
    if file_name:
        deci = "{0:.5f}"  # number of decimal points
        end = len(np_list[0])
        for a_row in np_list:
            info = ''
            info = (str(int(a_row[0]))      + '\t'
                    + deci.format(a_row[1]) + '\t'
                    + str(int(a_row[2]))    + '\t'
                    + deci.format(a_row[3]) + '\t'
                    + str(int(a_row[4]))    + '\t'
                    + str(a_row[5])         + '\t'
                    + str(int(a_row[6]))
                    )
            for i in range(sV._headers, end):
                if a_row[i] == 0: 
                    info = add_to_str(info, 0)
                else:
                    info = add_to_str(info, a_row[i])
            file_name.write(info + '\n')
    else:
        print('Something went wrong while printing from np.')

def writeToFile(FileName, IonObject, theSearch):
    """ Print the following details from each IonObject: scan number, 
        mass of the original peptide, charge, calculated mass, retention time,
        maximum intensity, number of "fragments" found and the intensity for each value we are looking for.
    """
    if FileName and IonObject:
        deci = "{0:.5f}"  # number of decimal points
        info = (str(IonObject.scanNo) + '\t'
                + deci.format(IonObject.pepmass.mz) + '\t'
                + str(IonObject.charge) + '\t'
                + deci.format(IonObject.Mass) + '\t'
                + str(IonObject.RT) + '\t'
                + str(IonObject.MaxInts) + '\t'
                + str(IonObject.fragmentCount)
                )

        fList = IonObject.fragments
        for o in theSearch.oxo_list:
            if fList.get(o, False):
                info = add_to_str(info, fList[o][1])
            else:
                info = add_to_str(info, 0)

        # Print value if found for each value in theSearch list, print 0 if not found. 
        # Same rule for doubly and triply but must be checked separately 
        # as should only print anything if option is selected.
        for ap in theSearch.pp_list: 
            if fList.get(ap, False) and fList[ap].get(sV._single, False): 
                    info = add_to_str(info, fList[ap][sV._single])
            else:
                info = add_to_str(info, 0)
                    
            if theSearch.doubly:
                if (fList.get(ap, False) and fList[ap].get(sV._double, False)): 
                        info = add_to_str(info, fList[ap][sV._double])
                else:  
                    info = add_to_str(info, 0)        
                
            if theSearch.triply:
                if (fList.get(ap, False) and fList[ap].get(sV._triple, False)): 
                        info = add_to_str(info, fList[ap][sV._triple])
                else:
                    info = add_to_str(info, 0)

        
        FileName.write(info + '\n')
    else:
        print('invalid objects')

def writeHeaders(FileName, theSearch):
    """ Print headers for peptides.
    """
    if FileName:
        header = ('SCAN\t'
                  + 'MZ\t'
                  + 'CHARGE\t'
                  + 'Calc MASS\t'
                  + 'RT\t'
                  + 'MAX Intens\t'
                  + '# found\t'
                  )

        for o in theSearch.oxo_list:
            header = (header + str(o) + '\t')
        
        for p in theSearch.pp_list:
            header = (header + str(p))
            if theSearch.doubly:
                header = (header + '\t' + str(p) + '_2')
            if theSearch.triply:
                header = (header + '\t' + str(p) + '_3')
            header = (header + '\t')
        header = (header + '\n')

        FileName.write(header)
    else:
        print('Invalid filename: ' + str(FileName))

def stripLine(aline):
    """ Returns only what comes after '='. 
    """
    return aline.split('=')[1].strip()

def pepLine(aLine):
    """ Split the string of a fragment into it's m/z
        and intensity float values.
        :return: list with an m/z and intensity value
        :rtype: list 
    """
    aLine = aLine.split(' ')
    return [float(aLine[0]), float(aLine[1])]

def add_to_str(print_string, a_value):
    """ Converts passed in value to string and adds at the
        end of passed in string, with a tab inbetween.
        :return: String with a tab, then value attached at the end.
        :rtype: string 
    """
    print_string = print_string + '\t' + str(a_value)
    return print_string


''' -----------------------------------------------------------
    Functions for mathematical and list processing.
    ----------------------------------------------------------- '''

def compare(tofind, value, tolerance):
    """ Compare whether two values are within
        the given tolerance.
        :return: True if within the tolerance, False if not.
        :rtype: boolean 
    """
    return math.fabs(value - tofind) < tolerance
    # return math(tofind, value, abs_tol = tolerance)

def calcTol(m_z, ppm):
    """ Calculate the tolerance for given m_z.
        :return: tolerance
        :rtype: float 
    """
    return (m_z * ppm) / 1000000

def ppm_tolerance(valuesList, ppm):
    """ Calculate the tolerance for each value in valuesList
        for the given ppm.
        :return: list of values, tolerance pairs
        :rtype: list 
    """
    pairs = []
    for m_z in valuesList:
        pairs.append([m_z, calcTol(m_z, ppm)])
    return pairs

def chargedMassVar(mz, n):
    """ Calculating the theoretical n-charged (doubly or triply)
        mass variation of passed in m/z value
        :return: n-charged mass
        :rtype: float
    """
    return (mz + (sV._Hplus * (n-1))) / n

def isIsotope(curr, pot, n):
    """ Compares curr(ent) and pot(ential) to see if pot is an isotope of curr.    
        e.g. curr + 0.5 +- tolerance
        :return: true if pot is curr's isotope.
        :rtype: boolean 
    """
    temp = math.fabs(pot-curr)
    return math.fabs(temp-sV.chType[n]) < sV._variance
