#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 12:06:52 2018
@author: xgotda
"""

import sys
from IonClass import Ions
from helperMethods import *
from SearchClass import DoSearch
import numpy as np
#from distutils.util import strtobool



# ********************************* SET VARIABLES ******************************
mgf_file = 'mgfFiles/small.mgf'
#mgf_file = 'mgfFiles/QEHF_180716_15.mgf'
file_write = 'out/tempApi.txt'

#  oxo = oxonium (glycan)
glycans = [204.08667, 274.0921, 366.1395]
glycan_ppm = 5

#  pp = peptide (larger fragment)
peptides = [1786.9487, 1990.0281, 2136.086]
peptide_ppm = 5

doubly_charged = True
triply_charged = True
# *****************************************************************************

def info():
    return "Starting processing..."

class Process():
    """ Parameters and processing. 
    """
    def __init__(self, mgf_file, save_file, 
                    glycans=[], glycan_ppm=0, 
                    peptides=[], peptide_ppm=0, 
                    doubly_charged=True, triply_charged=True,
                    zero_sum_gly=True, zero_sum_pep=True):
        """ Create a new processing instance and initialise all parameters. 
            Used in the :meth:'process_mgf' method.
            :param mgf_file: the location of the mgf file to be read
            :param save_file: the location and name of the output file to be created
            :param glycans: list of glycans we wish to find
            :param glycan_ppm: tolerance to find glycans within in ppm (parts per million) 
            :param peptides: list of peptides we wish to find
            :param peptide_ppm: tolerance to find peptides within in ppm (parts per million) 
            :param doubly_charged: include doubly charged peptides (default: True)
            :param triply_charged: include triply charged peptides (default: True)
            :param zero_sum_gly: Remove rows where the sum of glycans' intensities is zero (default True)
            :param zero_sum_pep: Remove rows where the sum of peptides' intensities is zero (default True)
        """
        self.mgf_file = mgf_file
        self.file_write = save_file
        self.glycans = glycans
        self.glycan_ppm = glycan_ppm
        self.peptides = peptides
        self.peptide_ppm = peptide_ppm
        self.doubly_charged = doubly_charged
        self.triply_charged = triply_charged
        self.zero_sum_gly = True
        self.zero_sum_pep = True

    def param_summary(self):
        return ('Mgf file: '    + str(self.mgf_file)        + '\n' +
                'Save as: '     + str(self.file_write)      + '\n' + 
                'Doubly: '      + str(self.doubly_charged)  + '\n' +
                'Triply: '      + str(self.triply_charged)  + '\n' + 
                'Glycans: '     + str(self.glycans)         + '\n' + 
                'Peptides: '    + str(self.peptides)        + '\n' + 
                'glycan ppm: '  + str(self.glycan_ppm)      + '\n' +
                'peptide ppm: ' + str(self.peptide_ppm)     + '\n' +
                'Include zero sum glycans' + str(self.zero_sum_gly) + '\n' +
                'Include zero sum peptides' + str(self.zero_sum_pep) + '\n' + ' .')

    def process_mgf(self):
        aSearch = DoSearch(self)
        linesRead = 0
        addedIon = 0
        records = 0
        toPrint = []

        try:
            with open(self.mgf_file, 'r') as rf:
                # print("opened read file")
                try:
                    with open(self.file_write, 'w') as wf:
                        # print("opened writefile")
                        line = 'start'
                        while line:
                            line = rf.readline()
                            linesRead += 1
                            if 'BEGIN' in line:
                                records += 1
                                newIon = Ions()
                                tempArr = []
                                line = rf.readline()
                                while 'END' not in line:
                                    if '=' in line:
                                        if 'TITLE' in line:
                                            newIon.title = stripLine(line)
                                        elif 'PEPMASS' in line:
                                            newIon.pepmass.frLine(line.split('=')[1])
                                        elif 'CHARGE' in line:
                                            newIon.charge = int(stripLine(line)[0])
                                        elif 'RTINSECONDS' in line:
                                            newIon.RT = int(stripLine(line))
                                        elif 'SCANS' in line:
                                            newIon.scanNo = int(stripLine(line))
                                    else:
                                        tempArr.append(pepLine(line))
                                    line = rf.readline()

                                np_start = np.array(tempArr)
                                newIon.MaxInts = max(np_start[:, 1])
                                newIon.npWorking = aSearch.reduce_search_list(np_start)
                                aSearch.search(newIon)

                                if newIon.valid:
                            #TODO: check for zero_sum here before adding? Or in search?
                                    addedIon += 1
                                    newIon.calculateMass()
                                    toPrint.append(newIon)
                                del newIon
                        writeHeaders(wf, aSearch)

                        npAll = np.array(processIons(toPrint, aSearch))
                        npFinal = self.filter_zero_sum(npAll)
                        np_to_file(wf, npFinal)
                        # for ion in toPrint:
                        #     writeToFile(wf, ion, aSearch)
                except IOError as ew:
                    raise IOError(ew)
            endMessage = ('Records processed: ' + str(records) + '. ')
            endMessage = endMessage + (' Added: ' + str(len(npFinal[:, 0])))
            return endMessage
        except IOError as er:
            raise IOError(er)
    
    def filter_zero_sum(self, npAll):
        """ Filters out results where the sum of glycan and/or peptide intensities is zero. 
            Controlled by checkboxes.
            :returns: filtered array
            :rtype: numpy array 
        """
        headers = sV._headers
        gly_len = len(self.glycans)
        glys = npAll[:, headers:headers + gly_len].sum(axis=1) > 0
        peps = npAll[:, headers + gly_len:].sum(axis=1) > 0
        filtered_npArray = npAll    # Default state of including everything
        if ((not self.zero_sum_gly) and (not self.zero_sum_pep)):
            # Include gly AND pep are not ticked
            # Remove both: remove gly_sum == 0, remove pep_sum == 0
            # print('Remove both gly and pep (both unticked).')
            flipped_gly = glys == 0
            flipped_pep = peps == 0
            ans = (flipped_gly + flipped_pep) == 0
            filtered_npArray = npAll[ans]
        elif not self.zero_sum_gly: 
            # 'Include pep' is ticked, but not 'Include gly'
            # Remove where gly_sum == 0
            # print('Remove only gly (pep ticked)')
            filtered_npArray = npAll[glys]
        elif not self.zero_sum_pep:
            # 'Include gly' is ticked, but not 'Include pep'
            # Remove where pep_sum == 0 
            # print('Remove only pep (gly ticked)')
            filtered_npArray = npAll[peps]

        return filtered_npArray


if __name__ == '__main__':
    #[mgf_file, file_write, glycans, glycan_ppm, peptides, peptide_ppm, doubly_charged, triply_charged] = sys.argv[1:]
    #process_mgf()
    print(info())
