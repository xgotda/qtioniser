#!/usr/bin/env python3

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox
from PyQt5.QtCore import QDir

import sys, yaml, io
import main_ionui, glycan_ui
import mgfMain, glycans
# , file_params

# Definitions of parameter file (yaml) headings
_input_file =  'input_file'
_output_file = 'output_file'
_glycans =     'glycans'
_peptides =    'peptides'

_glycan_ppm =  'glycan_ppm'
_peptide_ppm = 'peptide_ppm'
_doubly_charged = 'doubly_charged'
_triply_charged = 'triply_charged'
_zero_sum_gly = 'zero_sum_gly'
_zero_sum_pep = 'zero_sum_pep'

class Ioniser(QtWidgets.QMainWindow, main_ionui.Ui_Ioniser):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.process_btn.clicked.connect(self.do_process)
        self.select_mgf_btn.clicked.connect(self.mgf_file)
        self.save_output_btn.clicked.connect(self.save_output)
        self.actionSelect_mgf_file.triggered.connect(self.mgf_file)
        self.actionSave_output_as.triggered.connect(self.save_output)
        self.actionAdd_Glycan.triggered.connect(self.add_glycans)
        self.actionLoad_params.triggered.connect(self.load_params)
        self.actionSave_params.triggered.connect(self.save_params)
        self.actionQuit.triggered.connect(self.close)
        self.actionHelp.triggered.connect(self.helpURL)
        self.glycans_values.installEventFilter(self)
        self.peptides_values.installEventFilter(self)
    
    
    def eventFilter(self, source, event):
        """ Deals with events Leave and FocusOut for glycan and peptide boxes. """
        if ((event.type() == QtCore.QEvent.Leave) or 
            (event.type() == QtCore.QEvent.FocusOut)):
            if source is self.glycans_values:
                self.update_box(self.glycans_values)
            elif source is self.peptides_values:
                self.update_box(self.peptides_values)
        return super(Ioniser, self).eventFilter(source, event)


    def update_box(self, values_box):
        """ Sorts and removes duplicates from the passed in text_box. """
        current = values_box.toPlainText().split('\n')
        current = self.sort_values(current)
        values_box.clear()
        for val in current:
            values_box.appendPlainText(val)
        values_box.appendPlainText('')
    
    def sort_values(self, values, rstr=True):
        """ Returns a sorted list of unique string values.
            :params: values: list or set with numbers (int, float or str)
                     rstr: return values as float or string (default = string)
            :return: sorted list of unique values (str)
            :rtype: list 
        """
        nvalues = []
        for x in values:
            if x: 
                try:
                    nvalues.append(float(x))
                except ValueError as ve:
                    print(ve)
        
        nvalues = sorted(list(set(nvalues)))
        if rstr:
            nvalues = [str(x) for x in nvalues]
        return nvalues
    
    def helpURL(self):
        link = 'https://qtioniser.readthedocs.io/en/latest/index.html'
        url = QtCore.QUrl(link)
        if not QtGui.QDesktopServices.openUrl(url):
            msgBox = QtWidgets.QMessageBox(self)
            msgBox.setWindowTitle('Info')
            msgBox.setTextFormat(QtCore.Qt.RichText)
            msg = 'Could not open help section.<br> Try <a href='+link+'>'+link+'.</a>'
            msgBox.setText(msg)
            msgBox.exec_()
    
    def add_glycans(self):
        """ Open separate window with glycan values. """
        gly_win = glycans.GlycanWindow(self)
        gly_win.show()
       
    def save_output(self):
        save_file = QFileDialog.getSaveFileName(self, 'Save output')[0]
        self.save_as.setText(QDir.toNativeSeparators(save_file))

    def mgf_file(self):
        mgf_file = QFileDialog.getOpenFileName(self, 'Open mgf file')[0]
        self.select_mgf.setText(QDir.toNativeSeparators(mgf_file))
    
    def load_params(self):
        params_file = QFileDialog.getOpenFileName(self, 'Open parameters file', filter='*.yaml')[0]
        if params_file:
            self.gui_from_file(params_file)

    def gui_from_file(self, ip_file):
        """ Read parameters from yaml file and set values in GUI.
            :param ip_file: input parameters file  
        """
        try:
            inp = yaml.safe_load(open(ip_file, 'r'))
            self.select_mgf.setText(inp[_input_file])
            self.save_as.setText(inp[_output_file])
    #TODO: use sort_values before adding to box
            for g in inp[_glycans]:
                self.glycans_values.appendPlainText(str(g))
            for p in inp[_peptides]:
                self.peptides_values.appendPlainText(str(p))
            
            self.glycan_ppm.setValue(inp[_glycan_ppm])
            self.peptide_ppm.setValue(inp[_peptide_ppm])
            self.doubly_chbx.setChecked(inp[_doubly_charged])
            self.triply_chbx.setChecked(inp[_triply_charged])
            self.zero_sum_gly.setChecked(inp[_zero_sum_gly])
            self.zero_sum_pep.setChecked(inp[_zero_sum_pep])
        except IOError as e: 
            QtWidgets.QMessageBox.warning(self, "Error", 
                "Unable to load all parameters from file.\n" + 
                "Please check parameter headings in the yaml file.\n\n" + 
                "Error: {0}".format(e))
        except KeyError as k:
            QtWidgets.QMessageBox.warning(self, "Error", 
            "Unable to load all parameters from file.\n" +
            "Please check that the keys are present and spelled correctly.\n\n" +
            "Error with key: {0}".format(k))
        self.statusbar.showMessage("Loaded file " + ip_file, 4000)

    def save_params(self):
        """ Save parameters to file, for quicker loading of values instead of
            input via GUI. 
        """
        # TODO: If save_p doesn't end with .yaml, add it.
        save_p = QFileDialog.getSaveFileName(self, 'Save parameters',  '*.yaml')[0]
        try:
            [gly, pep] = self.p_input_to_list()
            params = {
                _input_file : self.select_mgf.text(),
                _output_file : self.save_as.text(),
                _glycans : gly, 
                _peptides : pep, 
                _glycan_ppm : self.glycan_ppm.value(),
                _peptide_ppm : self.peptide_ppm.value(),
                _doubly_charged : self.doubly_chbx.isChecked(),
                _triply_charged : self.triply_chbx.isChecked(),
                _zero_sum_gly : self.zero_sum_gly.isChecked(),
                _zero_sum_pep : self.zero_sum_pep.isChecked()
            }

            with io.open(save_p, 'w', encoding='utf8') as params_file:
                yaml.safe_dump(params, params_file, allow_unicode=True)
        except:
            QtWidgets.QMessageBox.warning(self, "Error", 
                "Unable to save parameters to file. \nPlease check parameter headings.")
            return
        self.statusbar.showMessage("Parameters saved as " + save_p, 4000)

    def p_input_to_list(self):
        """ Converts glycan and peptide values from GUI input to lists. 
            Returns empty list if fails.
            :return: List with glycans (list) and peptides (list)
            :rtype: List of Lists
        """
        try:
            glycan_string = self.glycans_values.toPlainText()
            glycansL = [float(val) for val in glycan_string.split('\n') if val]
            
            peptide_string = self.peptides_values.toPlainText()
            peptidesL = [float(val) for val in peptide_string.split('\n') if val]
            return[glycansL, peptidesL]
        except:
            QtWidgets.QMessageBox.warning(self, "Error", 
                                        "Non-numerical values found in peptide and/or glycan field.")
            return 


    def do_process(self):
        self.statusbar.showMessage("Reading mgf file...", 4000)
        
        p_list = self.p_input_to_list()
        if p_list:
            [glycans, peptides] = p_list
        else:
            self.statusbar.showMessage("Error loading glycan and/or peptide values.", 6)
            return

        try:
            self.statusbar.showMessage(mgfMain.info(), 3000)
            a_process = mgfMain.Process(self.select_mgf.text(), 
                                        self.save_as.text(), 
                                        glycans=glycans,
                                        glycan_ppm=self.glycan_ppm.value(), 
                                        peptides=peptides, 
                                        peptide_ppm=self.peptide_ppm.value()) 
            a_process.doubly_charged = self.doubly_chbx.isChecked()
            a_process.triply_charged = self.triply_chbx.isChecked()
            a_process.zero_sum_gly = self.zero_sum_gly.isChecked()
            a_process.zero_sum_pep = self.zero_sum_pep.isChecked()
            
            # information = a_process.param_summary()
            # print(information)
            end_msg =  a_process.process_mgf()
            self.statusbar.showMessage(end_msg)
        except IOError as e:
            self.statusbar.clearMessage()
            # print(e)
            err_msg = "Something went wrong.\nCheck the input parameters.\n\n" + str(e)
            QtWidgets.QMessageBox.warning(self, "Error", err_msg)
            # QtWidgets.QMessageBox.information(self, "Summary", information)
            

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    ioniser = Ioniser()
    ioniser.show()
    sys.exit(app.exec_())
