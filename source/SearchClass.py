#!/usr/bin/env python3
"""
Created on Tue Nov 27 12:56:27 2018

@author: xgotda
"""

import numpy
import PeptideClass as pc
from helperMethods import *


class DoSearch:
    """docstring for DoSearch."""

#   def __init__(self, glycans=[], glycan_ppm=0,
#                  peptides=[], peptide_ppm=0):
    def __init__(self, pro_obj):
        self.oxo_list = sorted(pro_obj.glycans)
        self.oxo_ppm  = pro_obj.glycan_ppm
        self.pp_list  = sorted(pro_obj.peptides)
        self.pp_ppm   = pro_obj.peptide_ppm
        self.doubly   = pro_obj.doubly_charged
        self.triply   = pro_obj.triply_charged
        self.mc_list   = []     # mc = multi charged
        self.find_list = []
        self.initGlycans()
        self.initPeptides()
        self.initPotentials()
        self.setMinMax()

    def setMinMax(self):
        """ Sets the lowest and highest values to be found based on the combined lists of 
            glycans, peptides and multi-charged peptides. Sets both as zero if none of 
            the lists are populated. 
        """
        n = 2.5
        try:
            self.min = min(self.oxo_list + self.mc_list + self.pp_list) - n
        except:
            self.min = 0
        try:
            self.max = max(self.oxo_list + self.mc_list + self.pp_list) + n
        except:
            self.max = 0

    def initGlycans(self):
        if self.oxo_list:
            for o in self.oxo_list:
                aGlycan = pc.FindPep(o, calcTol(o, self.oxo_ppm))
                self.find_list.append(aGlycan)

    def initPeptides(self):
        if self.pp_list:
            for p in self.pp_list:
                aPep = pc.FindPep(p, calcTol(p, self.pp_ppm), pc._P)
                self.find_list.append(aPep)

    def initPotentials(self):
        """ Create a list of doubly and/or triply charged values (isotopes). 
        """
        if self.pp_list:
            x = 2
            y = 4
            
            if not self.doubly:
                x = 3
            if not self.triply:
                y = 3
            if x == y:
                # print('No isotopes to find.')
                return

            for parent in self.pp_list:
                for n in range(x, y):
                    mz = chargedMassVar(parent, n)
                    self.mc_list.append(mz)
                    aPotential = pc.FindMcPep(parent, mz, calcTol(mz, self.pp_ppm), n)
                    self.find_list.append(aPotential)

    def reduce_search_list(self, npArray):
        """ Reduces the list to be searched to be only within the values we want to find. 
            :returns: Search list between the min and max values we wish to find.
            :rtype: np.array 
        """
        npTop = npArray[numpy.nonzero(self.min < npArray[:, 0])[0], :]
        npReduced = npTop[numpy.nonzero(npTop[:, 0] < self.max)[0], :]
        return npReduced

    def search(self, anIon):
        for i in range(len(anIon.npWorking)):
            [curr_mz, curr_itsy] = anIon.npWorking[i]
            for f in self.find_list:
                if compare(f.mz, curr_mz, f.tol):
                    mz_Key = f.mz
                    if f.ptype != pc._G:
                        curr_itsy = max(curr_itsy,
                                        self.check(anIon, i, f.chtype))
                    if f.ptype == pc._M:
                        mz_Key = f.parentPep
                    anIon.addFragment(mz_Key, curr_itsy, f)
                    break

    def check(self, anIon, pos, chType):
        """ Checks for isotopes of the current mz.
            :returns: The intensity of the isotope if found, and zero if no isotope is found.
            :rtype: float  
        """
        currmz = anIon.npWorking[pos][0]
        toReturn = 0
        for i in range(1, 3):
            if pos+i < len(anIon.npWorking):
                [mz, insy] = anIon.npWorking[pos+i]
                if isIsotope(currmz, mz, chType):
                    toReturn = max(toReturn, insy)
                    currmz = mz
        return toReturn
