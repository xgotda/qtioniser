import sys
sys.path.insert(0, 'c:\\Users\\xgotda\\Code\\qtioniser\\')
print(sys.path)

import unittest
import SearchClass as sc
import PeptideClass as pc
import IonClass as ic
import helperMethods as h
import staticVariables as sV
import numpy as np
from mgfMain import Process


''' oxo = oxonium (glycan) '''
_oxo_ppm = 5
_findOxo = [147.0652, 204.08667, 366.1395]
_oxo_tolPairs = h.ppm_tolerance(_findOxo, _oxo_ppm)
_lenOxo = len(_findOxo)

'''' pp = peptide (larger fragment) '''
_pp_ppm = 5
_findPP = [1786.9487, 1990.0281, 2136.086]
_pp_tolPairs = h.ppm_tolerance(_findPP, _pp_ppm)
_lenPP = len(_findPP)

''' cm = doubly or triply charged mass'''
cm = []
for f in _findPP:
    for n in range(2, 4):
        cm.append(h.chargedMassVar(f, n))
cm_tolPairs = h.ppm_tolerance(cm, _pp_ppm)

print("****CM pairs:*****")
for c in cm_tolPairs:
    print(c)

_fake_mgf_oxo = [[_findOxo[0], 0], [1234, 123.4], [_findOxo[2], 366], [3333, 333]]
_fake_mgf_pp  = [[_findPP[0], 1000], [1234, 12.34], [_findPP[1], 202], [3333, 333]]
_fake_mgf_ppd = _fake_mgf_pp + [[cm_tolPairs[0][0], 999]] # added _findPP[0]
_fake_mgf_ppt = _fake_mgf_pp + [[cm_tolPairs[3][0], 30000]]
print(_fake_mgf_ppd)
print(_fake_mgf_ppt)
print('\n')

class TestDoSearch(unittest.TestCase):

    def setUp(self):
        empty_process = Process('', '')
        self.dds = sc.DoSearch(empty_process)
        # a = all, both doubly and triply
        a_process = Process('', '', glycans=_findOxo, glycan_ppm=_oxo_ppm,
                            peptides=_findPP, peptide_ppm=_pp_ppm, 
                            doubly_charged=True, triply_charged=True)
        self.ds = sc.DoSearch(a_process)
        
        # d = doubly only
        d_process = Process('', '', glycans=_findOxo, glycan_ppm=_oxo_ppm,
                            peptides=_findPP, peptide_ppm=_pp_ppm, 
                            doubly_charged=True, triply_charged=False)
        self.ds_d = sc.DoSearch(d_process)
        
        # t = triply only
        t_process = Process('', '', glycans=_findOxo, glycan_ppm=_oxo_ppm,
                            peptides=_findPP, peptide_ppm=_pp_ppm, 
                            doubly_charged=False, triply_charged=True)
        self.ds_t = sc.DoSearch(t_process)
        
        # n = none, neither doubly or triply
        n_process = Process('', '', glycans=_findOxo, glycan_ppm=_oxo_ppm,
                            peptides=_findPP, peptide_ppm=_pp_ppm, 
                            doubly_charged=False, triply_charged=False)
        self.ds_n = sc.DoSearch(n_process)
        
    def tearDown(self):
        del self.dds
        del self.ds

    def test_defaultInitSearch(self):
        self.assertEqual(self.dds.oxo_list, [],
                         'default oxo_list not empty')
        self.assertEqual(self.dds.pp_list, [],
                         'default oxo_list not empty')
        self.assertEqual(self.dds.oxo_ppm, 0,
                         'default oxo_list not empty')
        self.assertEqual(self.dds.pp_ppm, 0,
                         'default oxo_list not empty')

    def test_initSearch(self):
        self.assertEqual(self.ds.oxo_list, _findOxo,
                         'oxo_list not initialised correctly with given values')
        self.assertEqual(self.ds.pp_list, _findPP,
                         'pp_list not initialised correctly with given values')
        self.assertEqual(self.ds.oxo_ppm, _oxo_ppm,
                         'oxo_ppm not initialised correctly with given values')
        self.assertEqual(self.ds.pp_ppm, _pp_ppm,
                         'pp_ppm not initialised correctly with given values')

    def test_numberSearchList(self):
        numberOfValues = (_lenOxo + _lenPP
                          + (_lenPP * 2))  # 2ly & 3ly charged for _findPP
        self.assertEqual(len(self.ds.find_list), numberOfValues,
                         'wrong number of values in find_list')

    def test_initGlycans(self):
        for i in range(_lenOxo):
            dsi = self.ds.find_list[i]
            self.assertEqual(dsi.ptype, pc.pType[pc._G],
                             'Wrong ptype for Glycan')
            self.assertEqual(dsi.chtype, sV.chType[sV._single],
                             'Wrong charge type for Glycan')
            self.assertEqual([dsi.mz, dsi.tol], _oxo_tolPairs[i],
                             'incorrect Glycans in find_list.')

    def test_initPeptides(self):
        for i in range(_lenOxo, _lenPP):
            a = i-_lenOxo
            dsi = self.ds.find_list[_lenOxo+i]
            self.assertEqual(dsi.ptype, pc.pType[pc._P],
                             'Wrong ptype for Glycan')
            self.assertEqual(dsi.chtype, sV.chType[sV._single],
                             'Wrong charge type for Peptide')
            self.assertEqual([dsi.mz, dsi.tol], _pp_tolPairs[a],
                             'incorrect Peptides in find_list.')

    def test_initPotentials(self):
        for i in range(_lenPP, _lenPP*2):
            a = i-_lenOxo
            dsi = self.ds.find_list[_lenPP+i]
            self.assertEqual(dsi.ptype, pc.pType[pc._M],
                             'Wrong ptype for Multi-charged')
            self.assertNotEqual(dsi.chtype, sV._single,
                                'Wrong charge type for Multi-charged')
            if i % 2:
                self.assertEqual(dsi.chtype, sV._double,
                                 'Multi-charged not doubly charged')
            else:
                self.assertEqual(dsi.chtype, sV._triple,
                                 'Multi-charged not triply charged')
            self.assertEqual([dsi.mz, dsi.tol], cm_tolPairs[a],
                             'incorrect Potentials in find_list.')

    def test_search_glycans(self):
        ion = ic.Ions()
        np_start = np.array(_fake_mgf_oxo)
        ion.npWorking = self.ds.reduce_search_list(np_start)
        self.ds.search(ion)
        print('FOUND: ' + str(ion.fragments))
        self.assertIn(_fake_mgf_oxo[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_oxo[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        del ion

    def test_search_peptides(self):
        ion = ic.Ions()
        np_start = np.array(_fake_mgf_pp)
        ion.npWorking = self.ds.reduce_search_list(np_start)
        self.ds.search(ion)
        print('FOUND: ' + str(ion.fragments))
        self.assertIn(_fake_mgf_pp[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_pp[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        del ion

    def test_doubly_charged(self):
        """ Test for doubly charged peptides with the option turned on and off. """
        ion = ic.Ions()
        np_start = np.array(_fake_mgf_ppd)
        ion.npWorking = self.ds.reduce_search_list(np_start)
        self.ds.search(ion)
        print('FOUND (ALL)  : ' + str(ion.fragments))
        self.assertIn(_fake_mgf_ppd[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[0][0]].values()),
                      'doubly charged fragment not added to fragment list.')              
        del ion

        ion = ic.Ions()
        np_start = np.array(_fake_mgf_ppd)
        ion.npWorking = self.ds_d.reduce_search_list(np_start)
        self.ds_d.search(ion)
        print('FOUND (DOUBLY): ' + str(ion.fragments))
        self.assertIn(_fake_mgf_ppd[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[0][0]].values()),
                      'doubly charged fragment not added to fragment list.')              
        self.assertNotIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[2][0]].values()),
                      'doubly charged fragment not added to fragment list.')      
        del ion

        ion = ic.Ions()
        np_start = np.array(_fake_mgf_ppd)
        ion.npWorking = self.ds_t.reduce_search_list(np_start)
        self.ds_t.search(ion)
        print('FOUND (TRIPLY): ' + str(ion.fragments))
        self.assertIn(_fake_mgf_ppd[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertNotIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[0][0]].values()),
                      'doubly charged fragment FALSELY added to fragment list.')              
        del ion
       
        ion = ic.Ions()
        np_start = np.array(_fake_mgf_ppd)
        ion.npWorking = self.ds_n.reduce_search_list(np_start)
        self.ds_n.search(ion)
        print('FOUND (NONE)  : ' + str(ion.fragments))
        self.assertIn(_fake_mgf_ppd[0][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertIn(_fake_mgf_ppd[2][0], list(ion.fragments.keys()),
                      'fragment not added to fragment list.')
        self.assertNotIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[0][0]].values()),
                      'doubly charged fragment FALSELY added to fragment list.')    
        self.assertNotIn(_fake_mgf_ppd[4][1], list(ion.fragments[_fake_mgf_ppd[2][0]].values()),
                      'doubly charged fragment FALSELY added to fragment list.')              
        del ion

    def test_triply_charged(self):
            """ Test for triply charged peptides with the option turned on and off. """
            ion = ic.Ions()
            np_start = np.array(_fake_mgf_ppt)
            ion.npWorking = self.ds.reduce_search_list(np_start)
            self.ds.search(ion)
            print('FOUND (ALL)  : ' + str(ion.fragments))
            self.assertIn(_fake_mgf_ppt[0][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[2][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[4][1], list(ion.fragments[_fake_mgf_ppt[2][0]].values()),
                        'triply charged fragment not added to fragment list.')              
            del ion

            ion = ic.Ions()
            np_start = np.array(_fake_mgf_ppt)
            ion.npWorking = self.ds_d.reduce_search_list(np_start)
            self.ds_d.search(ion)
            print('FOUND (DOUBLY): ' + str(ion.fragments))
            self.assertIn(_fake_mgf_ppt[0][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[2][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertNotIn(_fake_mgf_ppt[4][1], list(ion.fragments[_fake_mgf_ppt[2][0]].values()),
                        'triply charged fragment not added to fragment list.')              
            del ion

            ion = ic.Ions()
            np_start = np.array(_fake_mgf_ppt)
            ion.npWorking = self.ds_t.reduce_search_list(np_start)
            self.ds_t.search(ion)
            print('FOUND (TRIPLY): ' + str(ion.fragments))
            self.assertIn(_fake_mgf_ppt[0][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[2][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[4][1], list(ion.fragments[_fake_mgf_ppt[2][0]].values()),
                        'triply charged fragment FALSELY added to fragment list.')              
            del ion
            ion = ic.Ions()
            np_start = np.array(_fake_mgf_ppt)
            ion.npWorking = self.ds_n.reduce_search_list(np_start)
            self.ds_n.search(ion)
            print('FOUND (NONE)  : ' + str(ion.fragments))
            self.assertIn(_fake_mgf_ppt[0][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertIn(_fake_mgf_ppt[2][0], list(ion.fragments.keys()),
                        'fragment not added to fragment list.')
            self.assertNotIn(_fake_mgf_ppt[4][1], list(ion.fragments[_fake_mgf_ppt[2][0]].values()),
                        'triply charged fragment FALSELY added to fragment list.')              
            del ion


def suiteIsDoSearch():
    suite = unittest.TestSuite()
    suite.addTest(TestDoSearch('test_defaultInitSearch'))
    suite.addTest(TestDoSearch('test_initSearch'))
    suite.addTest(TestDoSearch('test_numberSearchList'))
    suite.addTest(TestDoSearch('test_initGlycans'))
    suite.addTest(TestDoSearch('test_initPeptides'))
    suite.addTest(TestDoSearch('test_initPotentials'))
    suite.addTest(TestDoSearch('test_search_glycans'))
    suite.addTest(TestDoSearch('test_search_peptides'))
    suite.addTest(TestDoSearch('test_doubly_charged'))
    suite.addTest(TestDoSearch('test_triply_charged'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suiteIsDoSearch())
    # unittest.main()
