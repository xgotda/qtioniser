:: Icon needs adding in both formats to be shown when running.

echo off
echo RUNNING PYINSTALLER
SET start_time= %DATE%  %TIME%
echo %start_time%
pyinstaller "source\ion_app.py" ^
--clean ^
-n Ioniser ^
--onefile --nowindow ^
--noconsole ^
--noconfirm  ^
--log-level=WARN ^
--add-data="icons\316.png;." ^
--add-data="icons\316.ico;." ^
--icon="icons\316.ico" ^
--hidden-import=PyQt5.sip ^
